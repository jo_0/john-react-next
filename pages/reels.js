import Head from "next/head"
import ColorBlock from "../components/ColorBlock"
import TitleText from "../components/TitleText"
import { useState } from "react"
import NavBar from "../components/NavBar"
import { Sling as Hamburger } from 'hamburger-react'

export default function Reels() {
  const reelLinks = {
    dance: "https://www.youtube.com/embed/OVdDjiMbjSc",
    fight: "https://www.youtube.com/embed/iqYVwag3N7Y", 
    vocal: "https://www.youtube.com/embed/fDyUaSpl9CM",
  }

  const [reelSource, setReelSource] = useState(reelLinks.dance)
  const [hover, setHover] = useState(null)
  const [drawerOpen, setDrawerOpen] = useState(false)

  return (
    <>
    <Head>
      <title>John Cavaseno | Reels</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="icon" type="image/gif/png" href="theater-icon-1.png" />
      <link rel="preconnect" href="https://fonts.googleapis.com"/> 
      <link rel="preconnect" href="https://fonts.gstatic.com"/>
      {/* <link href="https://fonts.googleapis.com/css2?family=Hahmlet:wght@300&display=swap" rel="stylesheet"/> */}
      <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@500&display=swap" rel="stylesheet"/>
    </Head> 

    <ColorBlock
    height='21vh'
    width='100vw'
    backgroundColor='#424242'
    placement={{
      top: 0,
      left: 0,
    }}
    />
    <ColorBlock
    height='100vh'
    width='12vw'
    backgroundColor='#6699CC'
    placement={{
      top: 0,
      left: '2vw',
    }}
    />

    <h1
      style={{
        width: '1px',
        wordWrap: 'break-word',
        fontFamily: `'Cinzel', serif`,
        whiteSpace: 'pre-wrap',
        fontSize: '12vh',
        color: '#FFF',
        position: 'absolute',
        zIndex: 1,
        top: '1vh',
        left: '6vw',
      }}
    >
      REELS
    </h1>

    <iframe 
      src={reelSource} 
      allow='encrypted-media' 
      id='danceReel'
      style={{
        height: '81vh',
        width: '81.8vw',
        position: 'absolute',
        bottom: '3.2vh',
        right: '2vw',
        // margin: '10vh 5vw',
      }}
    />

    <button 
    onClick={() => setReelSource(reelLinks.dance)}
    onMouseEnter={() => setHover('dance')}
    onMouseLeave={() => setHover(null)}
    >
      <TitleText
      fontFamily={`'Cinzel', serif`}
      color='#6699CC'
      placement={{
        top: '1vh',
        right: '59vw',
      }}
      size='6vw'
      text='Dance'
      extraStyle={{
        ...(hover === 'dance' ? {color: '#FFF'} : {}),
        ...(reelSource === reelLinks['dance'] ? {color: '#FFF'} : {}),
      }}
      />
    </button>

    <button 
    onClick={() => setReelSource(reelLinks.fight)}
    onMouseEnter={() => setHover('fight')}
    onMouseLeave={() => setHover(null)}
    >
      <TitleText
      fontFamily={`'Cinzel', serif`}
      color='#6699CC'
      placement={{
        top: '1vh',
        right: '33.3vw',
      }}
      size='6vw'
      text='Fight'
      extraStyle={{
        ...(hover === 'fight' ? {color: '#FFF'} : {}),
        ...(reelSource === reelLinks['fight'] ? {color: '#FFF'} : {}),
      }}
      />
    </button>

    <button 
    onClick={() => setReelSource(reelLinks.vocal)}
    onMouseEnter={() => setHover('vocal')}
    onMouseLeave={() => setHover(null)}
    >
    <TitleText
      fontFamily={`'Cinzel', serif`}
      color='#6699CC'
      placement={{
        top: '1vh',
        right: '5vw',
      }}
      size='6vw'
      text='Vocal'
      extraStyle={{
        ...(hover === 'vocal' ? {color: '#FFF'} : {}),
        ...(reelSource === reelLinks['vocal'] ? {color: '#FFF'} : {}),
      }}
    />
    </button>

    {/* hamburger/navigation */}
    { drawerOpen ? (<NavBar />) : null}

    <div style={{
      position: 'fixed',
      top: 0,
      right: 0,
      zIndex: 100,
      margin: '1.5vh 1vw',
      backgroundColor: '#FFF',
      // boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.2)',
    }}>
      <Hamburger 
        direction='left' 
        toggled={drawerOpen} 
        toggle={() => { setDrawerOpen(!drawerOpen) }} 
      />
      </div>
    </>
  )
}
