import Head from "next/head"
import ImageOfficial from "../components/ImageOfficial"
import ColorBlock from "../components/ColorBlock"
import TitleText from "../components/TitleText"
import NavBar from "../components/NavBar"
import { Sling as Hamburger } from 'hamburger-react'
import { useState } from "react"

export default function HSR() {
  const [drawerOpen, setDrawerOpen] = useState(false)

  return (
    <>
    <Head>
      <title>John Cavaseno | Headshot & Resume</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="icon" type="image/gif/png" href="theater-icon-1.png" />
      <link rel="preconnect" href="https://fonts.gstatic.com"/>
      {/* <link href="https://fonts.googleapis.com/css2?family=Londrina+Outline&display=swap" rel="stylesheet"/> */}
      {/* <link href="https://fonts.googleapis.com/css2?family=Bangers&family=Montserrat&display=swap" rel="stylesheet"/>  */}
      <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@500&display=swap" rel="stylesheet"/>
    </Head> 

    <ColorBlock
      height='100vh'
      width='50vw'
      // backgroundColor='#6699CC'
      backgroundColor='#424242'
      placement={{
        top: 0,
        left: 0,
      }}
    />

    {/* headshot text & pic */}
    <TitleText 
      fontFamily={`'Cinzel', serif`}
      // color='#6699CC'
      color='#FFF'
      placement={{
        top: 0,
        left: '6vw',
      }}
      zIndex={1}
      size='7vw'
      text='Headshot'
    />
    <a target='_blank' href='/retouch2.jpg'>
      <ImageOfficial 
        source="/retouch2.jpg" 
        alternate="John Cavaseno Headshot" 
        height='75vh'
        width='30vw'
        backgroundColor='#FFF'
        placement={{
          top: '10vh',
          left: '4.4vw',
        }}
      />
    </a>

    {/* resume text & pic */}
    <TitleText 
      fontFamily={`'Cinzel', serif`}
      color='#424242'
      placement={{
        bottom: 0,
        right: '11vw',
      }}
      size='7vw'
      text='Resume'
    />
    <a target='_blank' href='/jcResume.pdf'>
      <ImageOfficial
        source="/jcResume.png" 
        alternate="John Cavaseno Resume" 
        height='75vh'
        width='30vw'
        backgroundColor='#424242'
        placement={{
          bottom: '10vh',
          right: '5vw',
        }}
      />
    </a>

    {/* ampersant */}
    <TitleText 
      fontFamily={`'Cinzel', serif`}
      color='#6699CC'
      placement={{
        top: '34.8vh',
        left: '34.8vw',
      }}
      zIndex={1}
      size='12vw'
      padding='0 11vw 2vh 11vw'
      text='&'
    />

    {/* hamburger/navigation */}
    { drawerOpen ? (<NavBar />) : null}

    <div style={{
      position: 'fixed',
      top: 0,
      right: 0,
      zIndex: 100,
      margin: '1.5vh 1vw',
      backgroundColor: '#FFF',
      // boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.2)',
    }}>
      <Hamburger 
        direction='left' 
        toggled={drawerOpen} 
        toggle={() => { setDrawerOpen(!drawerOpen) }} 
      />
      </div>
    </>
  )
}