import Head from "next/head"
import ColorBlock from "../components/ColorBlock"
import TitleText from "../components/TitleText"
import NavBar from "../components/NavBar"
import { Sling as Hamburger } from 'hamburger-react'
import { createRef, useState } from "react"
import emailjs from 'emailjs-com'
import ReCAPTCHA from "react-google-recaptcha"

export default function Contact() {
  const [disabledSubmit, setDisabledSubmit] = useState(true)
  const [drawerOpen, setDrawerOpen] = useState(false)

  const recaptchaRef = createRef()

  function sendEmail(event) {
    event.preventDefault()

    emailjs.sendForm('service_j07lvpe', 'template_hcvk1ea', event.target, 'user_Mp54oOKQzjIke1JUzqve0')
      .then((result) => {
        alert(result.text + ', email sent!')
        setDisabledSubmit(true)
        document.getElementById("contact-form").reset()
        window.grecaptcha.reset()
      }, (error) => {
        alert(error.text)
      })
  }

  return (
    <>
    <Head>
      <title>John Cavaseno | Contact</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="icon" type="image/gif/png" href="theater-icon-1.png" />
      <link rel="preconnect" href="https://fonts.gstatic.com"/>
      <link href="https://fonts.googleapis.com/css2?family=Hahmlet:wght@300&display=swap" rel="stylesheet"/>
      <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@500&display=swap" rel="stylesheet"/>
    </Head> 

    <ColorBlock
    height='21vh'
    width='100vw'
    backgroundColor='#6699CC'
    placement={{
      top: 0,
      left: 0,
    }}
    />
    <ColorBlock
    height='21vh'
    width='100vw'
    backgroundColor='#6699CC'
    placement={{
      bottom: 0,
      left: 0,
    }}
    />

    <ColorBlock
    height='100vh'
    width='12vw'
    backgroundColor='#424242'
    placement={{
      top: 0,
      left: '2vw',
    }}
    />
    <ColorBlock
    height='100vh'
    width='12vw'
    backgroundColor='#424242'
    placement={{
      top: 0,
      right: '2vw',
    }}
    />

    <TitleText
    fontFamily={`'Cinzel', serif`}
    color='#FFF'
    placement={{
      top: '0vh',
      left: '17.5vw',
    }}
    size='8.6vw'
    text='Contact Page'
    />

    <div 
    style={{
      position: "absolute",
      top: '22vh',
      left: '17vw',
      fontFamily: `'Hahmlet', serif`
    }}
    >
    <form 
    className="contact-form" 
    id="contact-form" 
    onSubmit={sendEmail} 
    style={{
      display: 'grid',
      gridTemplate: `
      "nameLabel  " 5vh
      "nameInput"   5vh
      "emailLabel " 5vh
      "emailInput"  5vh
      "msgLabel   " 5vh
      "msgInput"    21vh
      "submit"      11vh
      /66vw
      `,
      alignItems: 'center',
    }}
    >
      <input type="hidden" name="contact_number"/>

      <label style={{gridArea: 'nameLabel'}}>
        Name
      </label>
      <input 
      type="text" 
      name="from_name" 
      style={{
        gridArea: 'nameInput',
        fontFamily: `'Hahmlet', serif`,
      }} 
      placeholder='Dan'
      />
      
      <label style={{gridArea: 'emailLabel'}}>
        Email
      </label>
      <input 
      type="email" 
      name="reply_to" 
      style={{
        gridArea: 'emailInput',
        fontFamily: `'Hahmlet', serif`,
      }} 
      placeholder='Dan@gmail.com'
      />
      
      <label style={{gridArea: 'msgLabel'}}>
        Message
      </label>
      <textarea 
      name="message" 
      style={{
        gridArea: 'msgInput',
        fontFamily: `'Hahmlet', serif`,
        alignSelf: 'stretch',
      }} 
      placeholder='Hi John! I want you to play Dan in Joseph and the Amazing Technicolor Dreamcoat.'
      />
      
      <ReCAPTCHA 
      sitekey='6Lf2nBAcAAAAAIXVZ61CDhkWa11lYpWn4VSJdFD7' 
      onChange={() => setDisabledSubmit(false)}
      onExpired={() => setDisabledSubmit(true)}
      style={{
        justifySelf: 'end',
        marginTop: '4vh'
      }}
      />

      <input 
      type="submit" 
      value="Send" 
      disabled={disabledSubmit}
      style={{
        gridArea: 'submit',
        fontFamily: `'Hahmlet', serif`,
      }}
      />
    </form>
    </div>

    { /* hamburger/navigation */}
    { drawerOpen ? (<NavBar />) : null}

    <div style={{
      position: 'fixed',
      top: 0,
      right: 0,
      zIndex: 100,
      margin: '1.5vh 1vw',
      backgroundColor: '#FFF',
      // boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.2)',
    }}>
      <Hamburger 
        direction='left' 
        toggled={drawerOpen} 
        toggle={() => { setDrawerOpen(!drawerOpen) }} 
      />
    </div>
    </>
  )
}