import Head from 'next/head'
import ImageOfficial from '../components/ImageOfficial'
import ColorBlock from '../components/ColorBlock'
import TitleText from '../components/TitleText'
import NavBar from "../components/NavBar"
import { Sling as Hamburger } from 'hamburger-react'
import { useEffect, useState } from "react"

export default function Gallery() {
  const [drawerOpen, setDrawerOpen] = useState(false)

  const picArray = [
    '/John_SKYE.jpg', 
    '/gallery/IMG_6016.jpg',
    '/gallery/IMG_6017.jpg',
    '/gallery/IMG_6096.jpg',
    '/gallery/IMG_6097.jpg',
    '/gallery/IMG_6109.jpg',
    '/gallery/IMG_6110.jpg',
    '/gallery/IMG_6111.jpg',
    '/gallery/IMG_6112.jpg',
    '/gallery/IMG_6129.jpg',
    '/gallery/IMG_6130.jpg',
    '/gallery/IMG_6370.jpg',
    '/gallery/IMG_6371.jpg',
    '/gallery/IMG_6372.jpg',
    '/gallery/IMG_6373.jpg',
    '/gallery/IMG_6377.jpg',
    '/gallery/IMG_6378.jpg',
    '/gallery/IMG_6379.jpg',
    '/gallery/IMG_6380.jpg',
    '/gallery/IMG_6422.jpg',
    '/gallery/IMG_6423.jpg',
    '/gallery/IMG_6424.jpg',
    '/gallery/IMG_6425.jpg',
    '/gallery/IMG_6426.jpg',
    '/gallery/IMG_6574.jpg',
    '/gallery/IMG_6722.jpg',
    '/gallery/IMG_6723.jpg',
    '/gallery/IMG_6725.jpg',
    '/gallery/IMG_6726.jpg',
    '/gallery/IMG_6727.jpg',
    '/gallery/IMG_6775.jpg',
    '/gallery/IMG_6803.jpg',
    '/gallery/IMG_6815.jpg',
    '/gallery/IMG_6829.jpg',
    '/gallery/IMG_6834.jpg',
    '/gallery/IMG_7192.jpg',
    '/gallery/IMG_7193.jpg',
    '/gallery/IMG_7194.jpg',
    '/gallery/John_Crazy0.jpg',
    '/gallery/John_Cats0.jpg',
  ]

  // const [firstGalleryIndex, setFirstGalleryIndex] = useState(0)
  // const firstGalleryShuffler = function() {
  //   if (firstGalleryIndex + 1 >= picArray.length) return setFirstGalleryIndex(0)
  //   setFirstGalleryIndex(firstGalleryIndex + 1)
  // }

  const [secondGalleryMidIndex, setSecondGalleryMidIndex] = useState(30)
  const secondGalleryShuffler= function(position) {
    let focus = secondGalleryMidIndex + position
    if (focus >= picArray.length) return focus - picArray.length
    else if (focus < 0) return picArray.length + focus
    else return focus
  }

  const secondGalleryIndices = [-2,-1,0,1,2]
  const secondGalleryOptions = secondGalleryIndices.map((num) => {

    const photoIndex = secondGalleryShuffler(num)
    const photoName = picArray[photoIndex]

    return (
      <a onClick={() => setSecondGalleryMidIndex(photoIndex)} key={photoName}>
        <ImageOfficial
        source={photoName}
        alternate={"John Cavaseno Gallery " + num}
        height='18vh'
        width='16vw'
        margin='0 0.5vw'
        extraStyle={{
          padding: '1vh 0.5vw',
          position: 'relative',
        }}
        />
      </a>
    )
  })

  // useEffect(() => {
  //   setTimeout(firstGalleryShuffler, 3000)
  // }, [firstGalleryIndex])

  return (
    <>
    <Head>
      <title>John Cavaseno | Gallery</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="icon" type="image/gif/png" href="theater-icon-1.png" />
      <link rel="preconnect" href="https://fonts.gstatic.com"/>
      {/* <link href="https://fonts.googleapis.com/css2?family=Londrina+Outline&display=swap" rel="stylesheet"/> */}
      {/* <link href="https://fonts.googleapis.com/css2?family=Bangers&family=Montserrat&display=swap" rel="stylesheet"/>  */}
      <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@500&display=swap" rel="stylesheet"/>
    </Head> 

    {/* top color block */}
    <ColorBlock
    height='40vh'
    width='100vw'
    // backgroundColor='#B31B1B' // maroon
    backgroundColor='#6699CC'
    placement={{
      top: 0,
      left: 0,
    }}
    />

    {/* middle color block */}
    <ColorBlock
    height='60vh'
    width='100vw'
    // backgroundColor='#B31B1B' // maroon
    backgroundColor='#424242'
    placement={{
      top: '70vh',
      left: 0,
    }}
    />

    {/* bottom color block */}
    <ColorBlock
    height='40vh'
    width='100vw'
    // backgroundColor='#B31B1B' // maroon
    backgroundColor='#6699CC'
    placement={{
      top: '160vh',
      left: 0,
    }}
    />

    {/* large GALLERY text */}
    <TitleText 
    fontFamily={`'Cinzel', serif`}
    color='#FFF'
    placement={{
      top: 0,
      left: '3vw',
    }}
    size={'24vh'}
    text={'Gallery'}
    />

    {/* first image (shuffle) */}
    <ImageOfficial
    source={picArray[0]}
    alternate="John Cavaseno main gallery pic"
    height='61vh'
    width='64vw'
    margin='0'
    placement={{
      bottom: '6vh',
      right: '6vw',
    }}
    />

    {/* second gallery */}
    <div
    id='secondGallery'
    style={{
      width: '90vw',
      height: '70vh',
      // boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.2)',
      // backgroundColor: '#FFF',
      position: 'absolute',
      top: '105vh',
      left: '5vw',
      display: 'flex',
      justifyContent: 'center',
    }}
    >
      <img 
      src={picArray[secondGalleryMidIndex]} 
      alt='John Cavaseno TBD' 
      style={{
        maxHeight: '69vh',
        maxWidth: '90vw',
        boxShadow: '0px 0px 10px 2px rgba(0, 0, 0, 0.6)',
      }}
      />
    </div>

    {/* second gallery options */}
    <div
    id='secondGalleryOptions'
    style={{
      width: '90vw',
      height: '20vh',
      position: 'absolute',
      top: '177vh',
      left: '5vw',
    }}
    >{secondGalleryOptions}</div>

      {/* hamburger & navigation */}
    { drawerOpen ? (<NavBar />) : null}
    <div style={{
      position: 'fixed',
      top: 0,
      right: 0,
      zIndex: 100,
      margin: '1.5vh 1vw',
      backgroundColor: '#FFF',
      // boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.2)',
    }}>
      <Hamburger 
        direction='left' 
        toggled={drawerOpen} 
        toggle={() => { setDrawerOpen(!drawerOpen) }} 
      />
    </div>
    </>
  )
}